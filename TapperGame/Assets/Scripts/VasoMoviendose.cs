﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VasoMoviendose : MonoBehaviour {

	// Use this for initialization
	Rigidbody2D rg;
	void Start () 
	{
		rg = GetComponent<Rigidbody2D>();
	}

	// Update is called once per frame
	void Update () 
	{
		rg.transform.Translate(new Vector3(-0.1f,0,0f));
	}

	void OnCollisionEnter2D(Collision2D objeto)//Cuando acaba de ser tocado
	{
		if (objeto.gameObject.name == "pos1" || objeto.gameObject.name == "pos2" || objeto.gameObject.name == "pos3" || objeto.gameObject.name == "pos4") 
		{
			if(fondo.puntuacion<=50)
				fondo.puntuacion= 0;
			else
				fondo.puntuacion-= 50;
		}
		Destroy (this.gameObject,1);
	}
}
