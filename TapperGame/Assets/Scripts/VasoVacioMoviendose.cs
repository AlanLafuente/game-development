﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VasoVacioMoviendose : MonoBehaviour {

	// Use this for initialization
	Rigidbody2D rg;
	void Start () {
		rg = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {

		rg.transform.Translate(new Vector3(0.01f,0,0f));
	}

	void OnCollisionEnter2D(Collision2D objeto)//Cuando acaba de ser tocado
	{
		if (objeto.gameObject.name == "PosVaso1" || objeto.gameObject.name == "PosVaso2" || objeto.gameObject.name == "PosVaso3" || objeto.gameObject.name == "PosVaso4") {
			Destroy (this.gameObject);
		}
		else if(objeto.gameObject.name == "cantinero"){
			fondo.puntuacion += 100;
			Destroy (this.gameObject);
		}else
			Physics2D.IgnoreCollision (GetComponent<Collider2D>(), objeto.collider);
	}
}
