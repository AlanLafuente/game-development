﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class enemigoTres : MonoBehaviour {

	// Use this for initialization
	Rigidbody2D rg;
	byte contador = 0;
	public GameObject vasoVacio;
	void Start () {
		rg = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		rg.transform.Translate(new Vector3(0.01f,0,0f));
	}
	void OnCollisionEnter2D(Collision2D objeto)//Cuando acaba de ser tocado
	{
		if(objeto.gameObject.name == "pos1" || objeto.gameObject.name == "pos2" || objeto.gameObject.name == "pos3" || objeto.gameObject.name == "pos4")
			Destroy (this.gameObject);

		if (objeto.gameObject.name == "PosVaso1" || objeto.gameObject.name == "PosVaso2" || objeto.gameObject.name == "PosVaso3" || objeto.gameObject.name == "PosVaso4") {
			Destroy (this.gameObject);
			Debug.Log ("Cliente inatendido");

			fondo.puntuacion = 0;
			Application.LoadLevel("Opening");
		}

		if(objeto.gameObject.name == "vaso(Clone)")
			contador++;
		if (contador >= 3) {
			Instantiate (vasoVacio, this.transform.position,new Quaternion());
			Destroy (this.gameObject);
		}
			
	}
}
