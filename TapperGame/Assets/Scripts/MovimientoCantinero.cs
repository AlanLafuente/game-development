﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoCantinero : MonoBehaviour {

	// Use this for initialization
	Rigidbody2D rg;
	public GameObject vaso;

	Transform posCant1;
	Transform posCant2;
	Transform posCant3;
	Transform posCant4;

	Transform posVaso1;
	Transform posVaso2;
	Transform posVaso3;
	Transform posVaso4;

	byte posicion = 4;

	void Start () {
		rg = GetComponent<Rigidbody2D>();

		posCant1 = GameObject.Find("PosCant1").transform;
		posCant2 = GameObject.Find("PosCant2").transform;
		posCant3 = GameObject.Find("PosCant3").transform;
		posCant4 = GameObject.Find("PosCant4").transform;

		posVaso1 = GameObject.Find("PosVaso1").transform;
		posVaso2 = GameObject.Find("PosVaso2").transform;
		posVaso3 = GameObject.Find("PosVaso3").transform;
		posVaso4 = GameObject.Find("PosVaso4").transform;
		
	}
	
	// Update is called once per frame
	void Update () {


		if (Input.GetKeyDown(KeyCode.UpArrow)) 
		{
			switch (posicion) 
			{
			case 1:
				this.gameObject.transform.position = posCant4.position;
				posicion = 4;
				break;
			case 2:
				this.gameObject.transform.position = posCant1.position;
				posicion = 1;
				break;
			case 3:
				this.gameObject.transform.position = posCant2.position;
				posicion = 2;
				break;
			case 4:
				this.gameObject.transform.position = posCant3.position;
				posicion = 3;
				break;
			}
		}
		if (Input.GetKeyDown(KeyCode.DownArrow)) 
		{
			switch (posicion) 
			{
			case 1:
				this.gameObject.transform.position = posCant2.position;
				posicion = 2;
				break;
			case 2:
				this.gameObject.transform.position = posCant3.position;
				posicion = 3;
				break;
			case 3:
				this.gameObject.transform.position = posCant4.position;
				posicion = 4;
				break;
			case 4:
				this.gameObject.transform.position = posCant1.position;
				posicion = 1;
				break;
			}
		}

		if (Input.GetKeyDown(KeyCode.Space)) 
		{
			switch (posicion) 
			{
			case 1:
				Instantiate (vaso, posVaso1.position,new Quaternion());
				break;
			case 2:
				Instantiate (vaso, posVaso2.position,new Quaternion());
				break;
			case 3:
				Instantiate (vaso, posVaso3.position,new Quaternion());
				break;
			case 4:
				Instantiate (vaso, posVaso4.position,new Quaternion());
				break;
			}
		}
	}

	void OnCollisionEnter2D(Collision2D objeto)//Cuando acaba de ser tocado
	{
		if(objeto.gameObject.name == "vasoVacio(Clone)")
			Debug.Log ("Vaso agarrado + 100 pts");
	}
}
