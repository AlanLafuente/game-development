﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class fondo : MonoBehaviour {

	// Use this for initialization
	public GameObject enemigo1;
	public GameObject enemigo2;
	public GameObject enemigo3;
	public GameObject enemigo4;
	public GameObject enemigo5;

	public Transform pos1;
	public Transform pos2;
	public Transform pos3;
	public Transform pos4;
	float tiempo = 0;

	public static int puntuacion = 0;
	public Text text;

	byte nivel = 1;
	void Start () 
	{

		pos1 = GameObject.Find("pos1").transform;
		pos2 = GameObject.Find("pos2").transform;
		pos3 = GameObject.Find("pos3").transform;
		pos4 = GameObject.Find("pos4").transform;
		//StartCoroutine (tiempo);
	}

	/*
	IEnumerator tiempo()
	{
		WaitForSeconds (0.5f);
	}
	*/
	// Update is called once per frame
	void Update ()
	{
		tiempo++;
		if (tiempo < 3000) {
			if (tiempo % 100 == 0) {	
				LanzarEnemigo (byte.Parse ((Random.Range (1, 6).ToString ())), byte.Parse ((Random.Range (1, 5).ToString ())));
			}
		} else {
			if (tiempo == 3800) {
				Debug.Log ("Termina el juego");
				//pasar de nivel
				if(nivel == 1)
					Application.LoadLevel("Level2");
				else
					Application.LoadLevel("Opening");
			}
		}	
		text.text = puntuacion.ToString() + " Pts";




		if (Input.GetKeyDown(KeyCode.Q)) {
			tiempo = 3000;
		}
	}

	void LanzarEnemigo(byte enemigo, byte puerta)
	{
		//Debug.Log ("enemigo " + enemigo + ", puerta "+ puerta);
		GameObject en = new GameObject();
		switch (enemigo) {
		case 1:
			en = enemigo1;
			break;
		case 2:
			en = enemigo2;
			break;
		case 3:
			en = enemigo3;
			break;
		case 4:
			en = enemigo4;
			break;
		case 5:
			en = enemigo5;
			break;
		}

		switch (puerta) {
		case 1:
			Instantiate (en, pos1.position,new Quaternion());
			break;
		case 2:
			Instantiate (en, pos2.position,new Quaternion());
			break;
		case 3:
			Instantiate (en, pos3.position,new Quaternion());
			break;
		case 4:
			Instantiate (en, pos4.position,new Quaternion());
			break;
		}

	}
}
